# Getting Started

To start the application, follow these steps:

1. Build the Docker containers:

   docker-compose build

2. Start the Docker containers:

    docker-compose up

3. Open your web browser and go to http://localhost:10107/api/doc to access the API documentation.