ARG CLI_IMAGE=serversideup/php:8.2-cli
ARG RUNTIME_IMAGE=serversideup/php:8.2-fpm-nginx
ARG APP_ENV="prod"

FROM ${CLI_IMAGE} as cli_base
ENV PHP_DATE_TIMEZONE="Europe/Warsaw"
RUN apt-get update \
    && apt-get install -y --no-install-recommends php8.2-pgsql \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*
RUN sed -i 's/zend.assertions = -1/zend.assertions = 1/g' /etc/php/8.2/cli/php.ini

FROM ${RUNTIME_IMAGE} as fpm_nginx_base
ENV AUTORUN_ENABLED=true
ENV AUTORUN_SYMFONY_MIGRATION=true
ENV PHP_DATE_TIMEZONE="Europe/Warsaw"
RUN apt-get update \
    && apt-get install -y --no-install-recommends php8.2-pgsql \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*
RUN sed -i 's/zend.assertions = -1/zend.assertions = 1/g' /etc/php/8.2/fpm/php.ini

COPY --chmod=755 s6-overlay/ /etc/s6-overlay/

FROM cli_base  as composer_base
ARG ENV
ENV AUTORUN_ENABLED=false
COPY ./api/composer.json ./api/composer.lock ./
RUN if [ "$APP_ENV" = "prod" ] ; then composer install --no-dev --no-scripts --no-autoloader --prefer-dist ; else composer install --no-scripts --no-autoloader --prefer-dist ; fi
COPY ./api .
RUN if [ "$APP_ENV" = "prod" ] ; then composer install --no-dev --prefer-dist ; else composer install --prefer-dist ; fi

FROM cli_base as cli
COPY --from=composer_base /var/www/html /var/www/html

FROM fpm_nginx_base as fpm_nginx
ENV SSL_MODE="off"
COPY --from=composer_base --chown=webuser:webgroup /var/www/html /var/www/html