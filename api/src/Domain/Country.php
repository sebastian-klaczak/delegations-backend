<?php

namespace App\Domain;

class Country
{
    public const PL = 'PL';
    public const DE = 'DE';
    public const GB = 'GB';

    private string $code;

    public function __construct(string $code)
    {
        $this->code = $code;
    }

    public function getCode(): string
    {
        return $this->code;
    }

}

