<?php

declare(strict_types=1);

namespace App\Domain\Delegation;

use DateTimeInterface;

class DelegationHoursPerDayCalculator
{
    public function calculateHours(DateTimeInterface $startDate, DateTimeInterface $endDate): array
    {
        $hoursPerDayDict = [];
        $currentDatetime = clone $startDate;

        while ($currentDatetime < $endDate) {
            $nextDay = clone $currentDatetime;
            $nextDay->modify('tomorrow');
            $hours = min($endDate, $nextDay)->diff($currentDatetime)->h;

            if ($hours === 0) $hours = 24;

            $dateStr = $currentDatetime->format('Y-m-d');
            if (!isset($hoursPerDayDict[$dateStr])) {
                $hoursPerDayDict[$dateStr] = 0;
            }

            $hoursPerDayDict[$dateStr] += $hours;
            $currentDatetime = $nextDay;
        }

        return $hoursPerDayDict;
    }
}

