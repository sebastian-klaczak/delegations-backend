<?php

declare(strict_types=1);

namespace App\Domain\Delegation;

use App\Domain\Country;
use App\Domain\Delegation\DelegationCostRule\CountryDelegationCostRule;
use App\Domain\Delegation\DelegationCostRule\LengthDelegationCostRule;
use App\Domain\Delegation\DelegationCostRule\MinimumHaoursInDayDelegationCostRule;
use App\Domain\Delegation\DelegationCostRule\WorkDayDelegationCostRule;
use App\Domain\Employee\Employee;
use DateTimeInterface;

class DelegationCostCalculator
{
    private Country $country;
    private Employee $employee;
    private DateTimeInterface $startDate;
    private DateTimeInterface $endDate;

    private DelegationHoursPerDayCalculator $delegationHoursCalculator;

    private CountryDelegationCostRule $countryDelegationCostRule;
    private LengthDelegationCostRule $lengthDelegationCostRule;
    private MinimumHaoursInDayDelegationCostRule $minimumDelegationCostRule;
    private WorkDayDelegationCostRule $workDayDelegationCostRule;

    public function __construct(
        DelegationHoursPerDayCalculator      $delegationHoursCalculator,
        CountryDelegationCostRule            $countryDelegationCostRule,
        LengthDelegationCostRule             $lengthDelegationCostRule,
        MinimumHaoursInDayDelegationCostRule $minimumDelegationCostRule,
        WorkDayDelegationCostRule            $workDayDelegationCostRule
    ) {
        $this->delegationHoursCalculator = $delegationHoursCalculator;
        $this->countryDelegationCostRule = $countryDelegationCostRule;
        $this->lengthDelegationCostRule = $lengthDelegationCostRule;
        $this->minimumDelegationCostRule = $minimumDelegationCostRule;
        $this->workDayDelegationCostRule = $workDayDelegationCostRule;
    }

    public function calculateDelegationCost(Delegation $delegation): int
    {

        $days = $this->delegationHoursCalculator->calculateHours(
            $delegation->getStartDate(),
            $delegation->getEndDate()
        );

        $cost = 0;
        $dayNo = 0;

        foreach ($days as $day => $h) {
            $dayNo++;
            $dayCost =
                $this->countryDelegationCostRule->calculateMultiplier($delegation)
                * $this->minimumDelegationCostRule->calculateMultiplier($h)
                * $this->lengthDelegationCostRule->calculateMultiplier($dayNo)
                * $this->workDayDelegationCostRule->calculateMultiplier(new \DateTime($day));
            $cost += $dayCost;
        }

        return $cost;
    }
}
