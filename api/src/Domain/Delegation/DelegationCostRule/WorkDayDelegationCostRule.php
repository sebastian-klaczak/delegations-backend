<?php

declare(strict_types=1);

namespace App\Domain\Delegation\DelegationCostRule;

use DateTime;

class WorkDayDelegationCostRule
{
    public function calculateMultiplier(DateTime $day): int
    {
        $dayOfWeek = $day->format('N');

       if ($dayOfWeek == 6 || $dayOfWeek == 7) {
           return 0;
       } else {
           return 1;
       }
    }
}
