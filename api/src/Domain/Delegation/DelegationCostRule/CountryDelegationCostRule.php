<?php

declare(strict_types=1);

namespace App\Domain\Delegation\DelegationCostRule;

use App\Domain\Country;
use App\Domain\Delegation\Delegation;

class CountryDelegationCostRule
{
    public function calculateMultiplier(Delegation $delegation): int
    {
        switch ($delegation->getCountry()->getCode()) {
            case Country::PL: return 10;
            case Country::DE: return 50;
            case Country::GB: return 75;
            default: return 0;
        }
    }
}
