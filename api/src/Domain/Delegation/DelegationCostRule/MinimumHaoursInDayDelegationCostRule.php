<?php

declare(strict_types=1);

namespace App\Domain\Delegation\DelegationCostRule;

class MinimumHaoursInDayDelegationCostRule
{
    public function calculateMultiplier(int $hoursInADay): int
    {
        if ($hoursInADay >= 8) {
            return 1;
        } else {
            return 0;
        }
    }
}
