<?php

declare(strict_types=1);

namespace App\Domain\Delegation\ValidationRule;

use App\Domain\Delegation\Delegation;
use App\Domain\Delegation\DelegationRepository;
use App\Domain\Employee\Employee;
use App\Domain\Employee\EmployeeRepository;

class EmployeeExistRule
{
    private DelegationRepository $delegationRepository;

    public function __construct (
        EmployeeRepository $employeeRepository
    ) {
        $this->employeeRepository = $employeeRepository;
    }

    public function check(
        Employee $employee
    ): bool {
        return $this->employeeRepository->exist(
            $employee
        );
    }
}
