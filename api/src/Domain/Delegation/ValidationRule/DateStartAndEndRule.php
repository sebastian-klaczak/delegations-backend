<?php

declare(strict_types=1);

namespace App\Domain\Delegation\ValidationRule;

use App\Domain\Delegation\Delegation;

class DateStartAndEndRule
{
    public function check(
        Delegation $delegation,
    ): bool {
        if ($delegation->getStartDate() < $delegation->getEndDate()) {
            return true;
        } else {
            return false;
        }
    }
}
