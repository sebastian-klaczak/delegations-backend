<?php

declare(strict_types=1);

namespace App\Domain\Employee;

interface EmployeeRepository
{
    public function create(): Employee;

    public function exist(
        \App\Domain\Employee\Employee $employee
    ): bool;

}
