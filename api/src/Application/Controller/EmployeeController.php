<?php

declare(strict_types=1);

namespace App\Application\Controller;

use App\Application\EmployeeManager;
use App\Application\Dto\Employee;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Method contains operations related to employee
 *
 * @OA\Tag(name="Employee")
 */
class EmployeeController extends AbstractController
{
    /**
     * @OA\Post(
     *     summary="Add new employee",
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Ok",
     *     @OA\JsonContent(
     *        ref=@Model(type=Employee::class)
     *     )
     * )
     *
     */
    #[Route('api/employee', name: 'employee_add', methods: ['POST'])]
    public function add(
        EmployeeManager $employeeManager,
        SerializerInterface $serializer
    ): JsonResponse {
        $employee = $employeeManager->createEmployee();

        return new JsonResponse(
            $serializer->serialize($employee, 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

}
