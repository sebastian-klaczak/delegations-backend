<?php

declare(strict_types=1);

namespace App\Application;

use App\Domain\Country;
use App\Domain\Delegation\Delegation;
use App\Domain\Delegation\ValidationRule\DateStartAndEndRule;
use App\Domain\Delegation\ValidationRule\EmployeeExistRule;
use App\Domain\Delegation\ValidationRule\OneDelegationOnTimeRule;
use App\Domain\Employee\Employee;
use App\Application\Repository\DelegationRepository;
use App\Application\Repository\EmployeeRepository;
use DateTime;

class DelegationValidator
{
    private EmployeeRepository $employeeRepository;
    private DelegationRepository $delegationRepository;

    public function __construct(
        EmployeeRepository $employeeRepository,
        DelegationRepository $delegationRepository
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->delegationRepository = $delegationRepository;
    }

    public function isValid(\App\Application\Dto\Delegation $delegation): bool
    {
        $employee = new Employee($delegation->employeeId);

        $delegation = new Delegation(
            new Employee($delegation->employeeId),
            new Country($delegation->country),
            new DateTime($delegation->start),
            new DateTime($delegation->end)
        );

        if (!(new EmployeeExistRule($this->employeeRepository))->check($employee)) {
            return false;
        }

        if (!(new DateStartAndEndRule())->check($delegation)) {
            return false;
        }

        if (!(new OneDelegationOnTimeRule($this->delegationRepository))->check(
            $delegation,
            $employee
        )) {
            return false;
        }

        return true;

    }

}
