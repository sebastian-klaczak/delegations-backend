<?php

declare(strict_types=1);

namespace App\Application\Repository;

use App\Application\Entity\Delegation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class DelegationRepository extends ServiceEntityRepository implements \App\Domain\Delegation\DelegationRepository
{
    private EmployeeRepository $employeeRepository;

    public function __construct(
        ManagerRegistry $registry,
        EmployeeRepository $employeeRepository
    ){
        $this->employeeRepository = $employeeRepository;
        parent::__construct($registry, Delegation::class);
    }

    public function create(
        \App\Domain\Delegation\Delegation $delegation,
        \App\Domain\Employee\Employee $employee
    ): \App\Domain\Delegation\Delegation {

        $employeeEntity = $this->employeeRepository->find($employee->getId());

        $delegationEntity = new Delegation();
        $delegationEntity->setStartDate($delegation->getStartDate());
        $delegationEntity->setEndDate($delegation->getEndDate());
        $delegationEntity->setEmployee($employeeEntity);
        $delegationEntity->setCountry($delegation->getCountry()->getCode());
        $this->_em->persist($delegationEntity);
        $this->_em->flush();

        return $delegation;
    }

    public function exist(
        \App\Domain\Delegation\Delegation $delegation,
        \App\Domain\Employee\Employee $employee
    ): bool {

        $result = $this->createQueryBuilder('d')
            ->select('COUNT(d.id)')
            ->andWhere('d.employee = :employee')
            ->andWhere('d.startDate < :end')
            ->andWhere('d.endDate > :start')
            ->setParameter('employee', $employee->getId())
            ->setParameter('start', $delegation->getStartDate())
            ->setParameter('end', $delegation->getEndDate())
            ->getQuery()
            ->getSingleScalarResult();

        return (bool) $result;
    }

}
