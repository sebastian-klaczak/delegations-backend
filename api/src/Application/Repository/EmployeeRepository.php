<?php

declare(strict_types=1);

namespace App\Application\Repository;

use App\Application\Entity\Employee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


class EmployeeRepository extends ServiceEntityRepository implements \App\Domain\Employee\EmployeeRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Employee::class);
    }

    public function create(): \App\Domain\Employee\Employee
    {
        $employeeEntity = new Employee();
        $this->_em->persist($employeeEntity);
        $this->_em->flush();

        return new \App\Domain\Employee\Employee($employeeEntity->getId());
    }

    public function exist(
        \App\Domain\Employee\Employee $employee
    ): bool {
        if ($this->find($employee->getId())) {
            return true;
        } else {
            return false;
        }
    }
}
