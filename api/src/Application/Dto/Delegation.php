<?php

declare(strict_types=1);

namespace App\Application\Dto;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     description="Delegation model",
 *     title="Delegation",
 *     required={"start", "end", "employeeId", "country"}
 * )
 */
class Delegation
{
    /**
     * @OA\Property(
     *     type="string",
     *     format="date-time",
     *     description="Start date and time",
     *     example="2023-11-01 08:00:00"
     * )
     */
    public string $start;

    /**
     * @OA\Property(
     *     type="string",
     *     format="date-time",
     *     description="End date and time",
     *     example="2023-11-03 16:00:00"
     * )
     */
    public string $end;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Employee ID",
     *     example=1
     * )
     */
    public int $employeeId;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Country code ISO 3166-1 alpha-2",
     *     example="PL"
     * )
     */
    public string $country;

}
