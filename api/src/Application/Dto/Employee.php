<?php

declare(strict_types=1);

namespace App\Application\Dto;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     description="Employee model",
 *     title="Employee",
 *     required={"id"}
 * )
 */
class Employee
{
    /**
     * @OA\Property(
     *     type="integer",
     *     description="Employee ID",
     *     example=1
     * )
     */
    public int $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

}
