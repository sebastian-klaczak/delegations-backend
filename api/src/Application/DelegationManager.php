<?php

declare(strict_types=1);

namespace App\Application;

use App\Domain\Country;
use App\Domain\Delegation\Delegation;
use App\Domain\Delegation\ValidationRule\DateStartAndEndRule;
use App\Domain\Delegation\ValidationRule\EmployeeExistRule;
use App\Domain\Delegation\ValidationRule\OneDelegationOnTimeRule;
use App\Domain\Employee\Employee;
use App\Application\Repository\DelegationRepository;
use App\Application\Repository\EmployeeRepository;
use DateTime;

class DelegationManager
{
    private EmployeeRepository $employeeRepository;
    private DelegationRepository $delegationRepository;

    public function __construct(
        EmployeeRepository $employeeRepository,
        DelegationRepository $delegationRepository
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->delegationRepository = $delegationRepository;
    }

    public function createDelegation(\App\Application\Dto\Delegation $delegation): Delegation
    {
        $delegation = new Delegation(
            new Employee($delegation->employeeId),
            new Country($delegation->country),
            new DateTime($delegation->start),
            new DateTime($delegation->end)
        );

        $employee = new Employee($delegation->getEmployee()->getId());

        return $this->delegationRepository->create($delegation, $employee);
    }

    /**
     * @return Delegation[]
     */
    public function readDelegation(\App\Application\Dto\Employee $employee): array
    {
        $delegationsEntity = $this->delegationRepository->findByEmployee($employee->id);

        $delegations = [];

        /** @var \App\Application\Entity\Delegation $delegationEntity */
        foreach ($delegationsEntity as $delegationEntity) {
            $delegations[] = new Delegation(
                new Employee($delegationEntity->getEmployee()->getId()),
                new Country($delegationEntity->getCountry()),
                $delegationEntity->getStartDate(),
                $delegationEntity->getEndDate()
            );
        }

        return $delegations;
    }
}
