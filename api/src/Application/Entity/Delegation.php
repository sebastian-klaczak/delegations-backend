<?php

declare(strict_types=1);

namespace App\Application\Entity;

use App\Application\Repository\DelegationRepository;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'delegation', schema: 'public')]
#[ORM\Entity(repositoryClass: DelegationRepository::class)]
class Delegation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'delegation_id', type: 'integer')]
    private int $id;

    #[ORM\Column(name: 'start_date', type: 'datetime')]
    private DateTimeInterface $startDate;

    #[ORM\Column(name: 'end_date', type: 'datetime')]
    private DateTimeInterface $endDate;

    #[ORM\Column(name: 'country', type: 'string')]
    private string $country;

    #[ORM\ManyToOne(targetEntity: Employee::class)]
    #[ORM\JoinColumn(name: 'employee_id', referencedColumnName: 'employee_id')]
    private Employee $employee;

    public function getId(): int
    {
        return $this->id;
    }

    public function getStartDate(): DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(DateTimeInterface $startDate): void
    {
        $this->startDate = $startDate;
    }

    public function getEndDate(): DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(DateTimeInterface $endDate): void
    {
        $this->endDate = $endDate;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    public function getEmployee(): Employee
    {
        return $this->employee;
    }

    public function setEmployee(Employee $employee): void
    {
        $this->employee = $employee;
    }

}
