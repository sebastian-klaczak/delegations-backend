<?php

declare(strict_types=1);

namespace App\Application;

use App\Domain\Employee\Employee;
use App\Application\Repository\EmployeeRepository;

class EmployeeManager
{
    private EmployeeRepository $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function createEmployee(): Employee
    {
        return $this->employeeRepository->create();
    }

}
