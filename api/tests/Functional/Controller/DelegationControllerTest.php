<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class DelegationControllerTest extends WebTestCase
{
    private KernelBrowser $client;

    public function testDelegation(): void
    {
        $this->client = self::createClient();

        $employeeId = $this->addEmployee();
        $this->addDelegation($employeeId);
        $this->getDelegation($employeeId);
    }

    private function addEmployee(): int
    {
        $this->client ->request('POST', '/api/employee');

        $this->assertEquals(Response::HTTP_OK, $this->client ->getResponse()->getStatusCode());

        $content = $this->client->getResponse()->getContent();
        $data = json_decode($content, true);

        $this->assertArrayHasKey('id', $data);

        return $data['id'];
    }

    private function addDelegation(int $employeeId)
    {
        $data['start'] = "2023-11-03 18:00:00"; //6h
        //sb
        //nd
        $data['end'] = "2023-11-06 16:00:00"; //16h
        $data['employeeId'] = $employeeId;
        $data['country'] = "GB";

        $this->client ->request(
            'POST',
            '/api/delegation',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals(Response::HTTP_OK, $this->client ->getResponse()->getStatusCode());
    }

    private function getDelegation(int $employeeId)
    {
        $this->client ->request('GET', '/api/delegation/'.$employeeId);

        $this->assertEquals(Response::HTTP_OK, $this->client ->getResponse()->getStatusCode());

        $content = $this->client ->getResponse()->getContent();
        $data = json_decode($content, true);

        $this->assertArrayHasKey('amountDue', $data[0]);
        $this->assertEquals(75, $data[0]['amountDue']);
    }

}


