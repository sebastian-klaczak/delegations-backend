<?php

declare(strict_types=1);

namespace App\Tests\Unit\Domain;

use App\Domain\Delegation\DelegationHoursPerDayCalculator;
use DateTime;
use PHPUnit\Framework\TestCase;

class DelegationHoursPerDayCalculatorTest extends TestCase
{
    /**
     * @dataProvider dateRangeProvider
     */
    public function testCalculateHoursPerDay($startDatetime, $endDatetime, $expectedResult)
    {
        $calculator = new DelegationHoursPerDayCalculator();
        $result = $calculator->calculateHours(new DateTime($startDatetime), new DateTime($endDatetime));
        $this->assertEquals($expectedResult, $result);
    }

    public static function dateRangeProvider(): array
    {
        return [
            ['2023-11-01 08:00:00', '2023-11-03 11:00:00', ['2023-11-01' => 16, '2023-11-02' => 24, '2023-11-03' => 11]],
            ['2023-11-01 08:00:00', '2023-11-01 15:00:00', ['2023-11-01' => 7]],
            ['2023-11-03 08:00:00', '2023-11-01 11:00:00', []],
        ];
    }
}


