<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231101073719 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE public.delegation_delegation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE public.employee_employee_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE public.delegation (delegation_id INT NOT NULL, employee_id INT DEFAULT NULL, start_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, end_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, country VARCHAR(255) NOT NULL, PRIMARY KEY(delegation_id))');
        $this->addSql('CREATE INDEX IDX_8C115A338C03F15C ON public.delegation (employee_id)');
        $this->addSql('CREATE TABLE public.employee (employee_id INT NOT NULL, PRIMARY KEY(employee_id))');
        $this->addSql('ALTER TABLE public.delegation ADD CONSTRAINT FK_8C115A338C03F15C FOREIGN KEY (employee_id) REFERENCES public.employee (employee_id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE public.delegation_delegation_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE public.employee_employee_id_seq CASCADE');
        $this->addSql('ALTER TABLE public.delegation DROP CONSTRAINT FK_8C115A338C03F15C');
        $this->addSql('DROP TABLE public.delegation');
        $this->addSql('DROP TABLE public.employee');
    }
}
